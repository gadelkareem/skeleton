package main

import (
	"backend/controllers"
	"backend/di"
	"backend/kernel"
	"backend/routers"
)

// Main is the entry point for the application. It bootstraps the application, initializes the dependency injection container, and runs the application.
func Main() {
	kernel.Bootstrap()      // Bootstraps the application.
	c := di.InitContainer() // Initializes the dependency injection container.
	kernel.App.RunCommand() // Runs the command, if any.

	routers.InitRouters(c)                                                                               // Initializes the routers.
	kernel.App.SetupServer(&controllers.ErrorController{ApiController: controllers.NewApiController(c)}) // Sets up the server.
	c.QueManager.StartWorkers()                                                                          // Starts the workers.
	kernel.App.Run()                                                                                     // Runs the application.
}
