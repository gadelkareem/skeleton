// Package controllers
package controllers

import (
	"bytes"
	"encoding/json"
	"net/http"

	"backend/di"
	"backend/internal"
	"backend/kernel"
	"backend/models"
	"backend/utils/paginator"
	"github.com/astaxie/beego/logs"
	"github.com/google/jsonapi"
)

// ApiController API controller is the base controller for all API controllers.
type (
	ApiController struct {
		BaseController
	}
)

// NewApiController Creates a new API controller.
func NewApiController(c *di.Container) ApiController {
	return ApiController{BaseController: BaseController{C: c}}
}

// Prepare is called before the all actions.
// It sets the CORS headers and checks the user permissions.
// It also checks the rate limit.
// It also sets the user in the context.
func (c *ApiController) Prepare() {
	c.EnableRender = false
	c.BaseController.Prepare()
	kernel.SetCORS(c.Ctx)
	c.setUser()

	u := c.Ctx.Request.URL.String()
	method := c.Ctx.Request.Method
	c.rbac(u, method)
	c.rateLimit(u, method)
}

// rateLimit Checks if the user has exceeded the rate limit.
// If the user has exceeded the rate limit it returns an error.
func (c *ApiController) rateLimit(u, method string) {
	ip := c.requestIP()
	b, err := c.C.RateLimiter.IsRateExceeded(c.user, ip, u, method)
	c.handleError(err)
	if b {
		logs.Error("rate exceeded for %s %s %s", ip, method, c.Ctx.Request.URL)
		c.handleError(internal.ErrTooManyRequests)
	}
}

// rbac Checks if the user has access to the given route.
// If the user does not have access it returns an error.
func (c *ApiController) rbac(u, method string) {
	if !c.C.RBAC.CanAccessRoute(c.user, u, method) {
		if c.user == nil {
			c.handleError(internal.ErrInvalidJWTToken)
		}
		c.handleError(internal.ErrForbidden)
	}
}

// parseRequest Parses the request body into the given model.
// If the request body is invalid it returns an error.
func (c *ApiController) parseRequest(m interface{}) {
	b := bytes.NewBuffer(c.Ctx.Input.RequestBody)
	if err := jsonapi.UnmarshalPayload(b, m); err != nil {
		logs.Error("Error parsing request %s err: %s", c.Ctx.Request.URL, err)
		c.handleError(internal.ErrInvalidRequest)
	}
}

// validate Validates the given model.
// If the model is invalid it returns an error.
func (c *ApiController) validate(m interface{}) {
	v := kernel.Validation()
	b, err := v.Valid(m)
	if err != nil {
		c.handleError(err)
	}
	if !b {
		vErrs := make(map[string]interface{})
		for _, e := range v.Errors {
			vErrs[e.Key] = e.Error()
		}
		c.handleError(internal.ValidationErrors(vErrs))
	}

	return
}

// parseJSONRequest Parses the request body into the given model.
// If the request body is invalid it returns an error.
func (c *ApiController) parseJSONRequest(m interface{}) {
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, m); err != nil {
		logs.Error("Error parsing request %s err: %s", c.Ctx.Request.URL, err)
		c.handleError(internal.ErrInvalidRequest)
	}
}

// jsonMany Marshals the given models into a JSON API response.
// It also sets the pagination links and meta.
// It also sets the status code to 200.
func (c *ApiController) jsonMany(p *paginator.Paginator) {
	c.Ctx.Output.Header("Content-Type", jsonapi.MediaType)
	c.Ctx.Output.SetStatus(http.StatusOK)

	for _, m := range p.Models {
		if ml, k := m.(models.BaseInterface); k {
			ml.Sanitize()
		}
	}
	pl, err := jsonapi.Marshal(p.Models)
	c.handleError(err)
	py := pl.(*jsonapi.ManyPayload)
	p.Optimize()
	py.Links = p.Links()
	py.Meta = p.Meta()

	b := bytes.NewBuffer(nil)
	err = json.NewEncoder(b).Encode(py)
	c.handleError(err)

	err = c.Ctx.Output.Body(b.Bytes())
	c.handleError(err)

	c.StopRun()
}

// json Marshals the given model into a JSON API response.
// It also sets the status code to 200.
func (c *ApiController) json(m interface{}) {
	c.Ctx.Output.Header("Content-Type", jsonapi.MediaType)
	c.Ctx.Output.SetStatus(http.StatusOK)

	if ml, k := m.(models.BaseInterface); k {
		ml.Sanitize()
	}

	b := bytes.NewBuffer(nil)
	err := jsonapi.MarshalPayload(b, m)
	c.handleError(err)

	err = c.Ctx.Output.Body(b.Bytes())
	c.handleError(err)

	c.StopRun()
}

// handleError Handles the given error.
// If the error is not an internal error it returns an internal error.
// It also sets the status code and the error response.
func (c *ApiController) handleError(err error) {
	if err == nil {
		return
	}
	internalErr, k := err.(internal.Error)
	if !k || internalErr == nil {
		logs.Error("Error: %s", err)
		internalErr = internal.ErrInternalError
	}
	c.Ctx.Output.Header("Content-Type", jsonapi.MediaType)
	c.Ctx.Output.SetStatus(internalErr.Status())
	b := bytes.NewBuffer(nil)
	e := jsonapi.MarshalErrors(b, []*jsonapi.ErrorObject{internalErr.Object()})
	if e != nil {
		logs.Error("Error unmarshal errors: %s", e)
	}
	e = c.Ctx.Output.Body(b.Bytes())
	if e != nil {
		logs.Error("Error writing body: %s", e)
	}
	go c.log(internalErr.Status())
	c.StopRun()
}

// SendStatus Sends the given status code.
// It also logs the status code.
func (c *ApiController) SendStatus(s int) {
	c.Ctx.ResponseWriter.WriteHeader(s)
	go c.log(s)
	c.StopRun()
}

// setUser Sets the user from the JWT token.
// If the token is invalid it returns an error.
func (c *ApiController) setUser() {
	tk := c.Ctx.Input.Header("Authorization")
	if tk == "" {
		return
	}

	p := c.C.JWTService.ParseHeader(tk)
	if p == nil {
		return
	}

	var err error
	c.user, err = c.C.UserService.UserById(p.UserId)
	if err != nil {
		logs.Error("Could not find user: %s", err)
	}
}

// auditLog Logs the given log.
// It also sets the user agent and the IP address.
func (c *ApiController) auditLog(l models.Log) {
	l.IP = c.requestIP()
	l.UserAgent = c.requestUserAgent()
	if l.Request == "" {
		l.Request = string(c.Ctx.Input.RequestBody)
	}
	if l.UserID != 0 && c.user != nil && l.UserID != c.user.ID {
		l.AdminID = c.user.ID
	}
	if l.UserID == 0 && c.user != nil {
		l.UserID = c.user.ID
	}
	c.C.AuditLogService.AddAuditLog(l)
}

// paginator Creates a new paginator.
// It also sets the page size, the page number, the sort, the filter and the URI.
func (c *ApiController) paginator(limit int) *paginator.Paginator {
	page := map[string]int{"size": limit, "after": 1}
	err := c.Ctx.Input.Bind(&page, "page")
	c.logOnError(err)
	return paginator.NewPaginator(page, c.readString("sort"), c.readString("filter"), c.Ctx.Input.URI())
}

// AssertCustomer Asserts that the given customer ID is the same as the user's customer ID.
// If the customer ID is not the same it returns an error.
func (c *ApiController) AssertCustomer(id string) {
	if id != c.user.CustomerID {
		c.handleError(internal.ErrForbidden)
	}
}
