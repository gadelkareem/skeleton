package controllers

import (
	"net/http"

	"backend/models"
)

// CommonController is the controller for common routes.
type CommonController struct {
	ApiController
}

// Contact sends an email to the admin.
// @router /contact [post]
func (c *CommonController) Contact() {
	r := new(models.Contact)
	c.parseRequest(r)
	c.validate(r)

	err := c.C.EmailService.Contact(r.Name, r.Email, r.Message, c.requestIP())
	c.handleError(err)

	c.SendStatus(http.StatusOK)
}
