package controllers

import (
	"backend/internal"
	"backend/kernel"
)

// ErrorController is the controller for error routes.
type (
	ErrorController struct {
		ApiController
	}
)

// Prepare prepares the error controller.
func (c *ErrorController) Prepare() {
	c.EnableRender = false
	kernel.SetCORS(c.Ctx)
}

// Error401 handles the 401 error.
func (c *ErrorController) Error401() {
	c.handleError(internal.ErrForbidden)
}

// Error400 handles the 400 error.
func (c *ErrorController) Error400() {
	c.handleError(internal.ErrInvalidRequest)
}

// Error403 handles the 403 error.
func (c *ErrorController) Error403() {
	c.handleError(internal.ErrForbidden)
}

// Error404 handles the 404 error.
func (c *ErrorController) Error404() {
	c.handleError(internal.ErrNotFound)
}

// Error429 handles the 429 error.
func (c *ErrorController) Error429() {
	c.handleError(internal.ErrTooManyRequests)
}

// Error500 handles the 500 error.
func (c *ErrorController) Error500() {
	c.handleError(internal.ErrInternalError)
}

// Error503 handles the 503 error.
func (c *ErrorController) Error503() {
	c.handleError(internal.ErrInternalError)
}

// ErrorDb handles the db error.
func (c *ErrorController) ErrorDb() {
	c.handleError(internal.ErrInternalError)
}
