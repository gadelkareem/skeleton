package controllers

import (
	"net/http"

	"backend/models"
)

// PaymentController is the controller for payments.
type PaymentController struct {
	ApiController
}

// Webhook handles the webhook from Stripe.
// @router /webhook/ [post]
func (c *PaymentController) Webhook() {
	r := &models.PaymentEvent{
		Payload:   c.Ctx.Input.RequestBody,
		Signature: c.Ctx.Request.Header.Get("Stripe-Signature"),
	}
	c.validate(r)

	err := c.C.PaymentService.Webhook(r)
	c.handleError(err)

	c.SendStatus(http.StatusOK)
}
