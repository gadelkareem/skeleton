package controllers

import "backend/kernel"

// ProductController is the controller for product routes.
type ProductController struct {
	ApiController
}

// GetProducts returns a list of products.
// @router / [get]
func (c *ProductController) GetProducts() {
	p, err := c.C.PaymentService.PaginateProducts(c.paginator(kernel.ListLimit))
	c.handleError(err)

	c.jsonMany(p)
}
