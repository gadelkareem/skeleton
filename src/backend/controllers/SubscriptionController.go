package controllers

import (
	"net/http"

	"backend/internal"
	"backend/models"
)

// SubscriptionController is the controller for subscriptions.
type SubscriptionController struct {
	ApiController
}

// CreateSubscription creates a subscription.
// @router / [post]
func (c *SubscriptionController) CreateSubscription() {
	r := new(models.Subscription)
	c.parseRequest(r)
	c.AssertCustomer(r.CustomerID)
	c.validate(r)

	s, err := c.C.PaymentService.CreateSubscription(r)
	c.handleError(err)

	go c.auditLog(models.Log{Action: "CreateSubscription"})

	c.json(s)
}

// UpdateSubscription updates a subscription.
// @router /:id [patch]
func (c *SubscriptionController) UpdateSubscription() {
	r := new(models.Subscription)
	c.parseRequest(r)
	c.assertCustomerHasSubscription(r.CustomerID, r.ID)
	c.validate(r)

	s, err := c.C.PaymentService.UpdateSubscription(r)
	c.handleError(err)

	go c.auditLog(models.Log{Action: "UpdateSubscription"})

	c.json(s)
}

// CancelSubscription cancels a subscription.
// @router /:id [delete]
func (c *SubscriptionController) CancelSubscription(id string) {
	c.assertCustomerHasSubscription(c.user.CustomerID, id)
	err := c.C.PaymentService.CancelSubscription(id, c.user.CustomerID)
	c.handleError(err)

	go c.auditLog(models.Log{Action: "CancelSubscription"})

	c.SendStatus(http.StatusNoContent)
}

// assertCustomerHasSubscription asserts that the customer has the subscription.
func (c *SubscriptionController) assertCustomerHasSubscription(customerID string, subscriptionID string) {
	c.AssertCustomer(customerID)
	if !c.C.PaymentService.CustomerHasSubscription(customerID, subscriptionID) {
		c.handleError(internal.ErrForbidden)
	}
}
