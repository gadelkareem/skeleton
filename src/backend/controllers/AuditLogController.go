package controllers

import (
	"backend/kernel"
)

// AuditLogController AuditLogController is the controller for audit logs.
type AuditLogController struct {
	ApiController
}

// GetAuditLogs returns the audit logs.
// @router / [get]
func (c *AuditLogController) GetAuditLogs() {
	p, err := c.C.AuditLogService.PaginateAuditLogs(c.paginator(kernel.ListLimit))
	c.handleError(err)

	c.jsonMany(p)
}
