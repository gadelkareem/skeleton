package controllers

import (
	"fmt"
	"net/http"

	"backend/internal"
	"backend/kernel"
	"backend/models"
	"backend/services"
)

// AuthController Auth Controller is the controller for authentication.
type AuthController struct {
	ApiController
}

// Token returns the access token and refresh token.
// @router /token [post]
func (c *AuthController) Token() {
	r := new(models.Login)
	c.parseRequest(r)
	c.validate(r)

	a, err := c.C.JWTService.Authenticate(r)
	c.handleError(err)

	if c.IsAjax() {
		c.addCookie(a.RefreshToken, services.RefreshCookieMaxAge)
	}

	c.json(a)
}

// RefreshToken returns a new access token and refresh token.
// @router /refresh-token [post]
func (c *AuthController) RefreshToken() {
	r := new(models.AuthToken)
	c.parseRequest(r)
	c.validate(r)

	a, err := c.C.JWTService.AuthenticateRefreshToken(r.RefreshToken)
	c.handleError(err)

	c.json(a)
}

// RefreshCookie returns a new access token and refresh token.
// @router /refresh-cookie [post]
func (c *AuthController) RefreshCookie() {
	if !c.IsAjax() {
		c.handleError(internal.ErrForbidden)
	}
	rt := c.Ctx.GetCookie(services.RefreshTokenCookieName)
	a, err := c.C.JWTService.AuthenticateRefreshToken(rt)
	c.handleError(err)

	c.json(a)
}

// Logout deletes the refresh token cookie.
// @router /logout [get]
func (c *AuthController) Logout() {
	if !c.IsAjax() {
		c.handleError(internal.ErrForbidden)
	}
	c.addCookie("", -1)

	c.SendStatus(http.StatusOK)
}

// SocialRedirect redirects the user to the social provider.
// @router /social/redirect [post]
func (c *AuthController) SocialRedirect() {
	r := new(models.SocialAuth)
	c.parseRequest(r)
	c.validate(r)

	a, err := c.C.SocialAuthService.Redirect(r.Provider)
	c.handleError(err)

	c.json(a)
}

// SocialCallback authenticates the user with the social provider.
// @router /social/callback [post]
func (c *AuthController) SocialCallback() {
	r := new(models.SocialAuth)
	c.parseRequest(r)
	c.validate(r)

	a, err := c.C.SocialAuthService.Authenticate(r)
	c.handleError(err)

	if c.IsAjax() {
		c.addCookie(a.RefreshToken, services.RefreshCookieMaxAge)
	}

	c.json(a)
}

// addCookie adds the refresh token cookie.
func (c *AuthController) addCookie(v string, exp int) {
	// max age time, path,domain, secure and httponly
	c.Ctx.SetCookie(services.RefreshTokenCookieName,
		v,
		exp,
		fmt.Sprintf("%s/auth/refresh-cookie", kernel.App.APIPath),
		kernel.App.Host,
		!kernel.IsDev(),
		true)
}
