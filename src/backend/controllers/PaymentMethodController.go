package controllers

import (
	"net/http"

	"backend/internal"
	"backend/models"
)

// PaymentMethodController is the controller for payment methods.
type PaymentMethodController struct {
	ApiController
}

// DeletePaymentMethod deletes a payment method.
// @router /:id [delete]
func (c *PaymentMethodController) DeletePaymentMethod(id string) {
	c.AssertCustomerHasPaymentMethod(c.user.CustomerID, id)
	err := c.C.PaymentService.DeletePaymentMethod(id, c.user.CustomerID)
	c.handleError(err)

	go c.auditLog(models.Log{Action: "DeletePaymentMethod"})

	c.SendStatus(http.StatusNoContent)
}

// CreatePaymentMethod creates a payment method.
// @router / [post]
func (c *PaymentMethodController) CreatePaymentMethod() {
	r := new(models.PaymentMethod)
	c.parseRequest(r)
	c.validate(r)
	_, err := c.C.PaymentService.AttachPaymentMethod(c.user.CustomerID, r)
	c.handleError(err)

	go c.auditLog(models.Log{Action: "CreatePaymentMethod"})

	c.SendStatus(http.StatusCreated)
}

// AssertCustomerHasPaymentMethod asserts that the customer has the payment method.
func (c *PaymentMethodController) AssertCustomerHasPaymentMethod(customerID string, paymentMethodID string) {
	if !c.C.PaymentService.CustomerHasPaymentMethod(customerID, paymentMethodID) {
		c.handleError(internal.ErrForbidden)
	}
}
