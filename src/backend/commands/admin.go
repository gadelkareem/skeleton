// Package commands contains all the commands that can be executed by the skeleton binary
/**
 * This file contains all the admin related commands.
 */
package commands

import (
	"fmt"

	"backend/kernel"
	"backend/models"
	"backend/services"

	h "github.com/gadelkareem/go-helpers"
)

// This is a command that can be called from the command line.
// It has a few subcommands:
// make-admin, activate-user, etc.
type admin struct {
	s *services.UserService
}

// NewAdmin returns a new admin command.
func NewAdmin(s *services.UserService) kernel.Command {
	return &admin{s: s}
}

// Run is the entry point for the Admin commands.
// It checks the first argument to determine which command to run.
func (c *admin) Run(args []string) {
	if len(args) > 1 {
		switch args[0] {
		case "make-admin":
			c.makeAdmin(args[1])
			return
		case "activate-user":
			c.activateUser(args[1])
			return
		}
	} else {
		c.Help()
	}
}

// makeAdmin makes the user identified by username an admin.
func (c *admin) makeAdmin(username string) {
	err := c.s.MakeAdmin(username)
	h.PanicOnError(err)
	fmt.Printf("User %s is now an admin\n", username)
}

// activateUser activates a user with the given username.
func (c *admin) activateUser(username string) {
	u, err := c.s.FindUser(&models.User{Username: username}, true)
	h.PanicOnError(err)
	u.Activate()
	err = c.s.Save(u, "active", "email_verify_hash")
	h.PanicOnError(err)
	fmt.Printf("User %s is now an active\n", username)
}

// Help prints the help message for the admin command.
func (c *admin) Help() {
	fmt.Printf(`
Usage: skeleton admin [options] ...

    Controls users

Available options:
    make-admin [username]  make a user an admin   
`)
}
