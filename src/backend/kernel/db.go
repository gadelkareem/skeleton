package kernel

import (
	"context"
	"database/sql"
	"time"

	"github.com/gadelkareem/go-helpers"

	"github.com/astaxie/beego/logs"
	"github.com/gadelkareem/que"
	"github.com/go-pg/pg/v9"
	"github.com/jackc/pgx/v4/pgxpool"
	_ "github.com/lib/pq"
)

// PgDb is the database struct.
type PgDb struct {
	*pg.DB
}

var (
	dbURL string
)

// InitDBConfig initializes the database config.
func initDBConfig() {
	dbURL = App.ConfigOrEnvVar("dbAddress", "DATABASE_URL")
}

// NewDB returns a new database connection.
func NewDB() *PgDb {
	db, err := newDb(dbURL)
	if err != nil {
		logs.Error("Database connection error: %s", err)
	}

	if IsDev() {
		db.EnableLogging()
	}

	return db
}

// EnableLogging enables logging for the database.
func (db *PgDb) EnableLogging() {
	db.AddQueryHook(dbLogger{})
}

// newDb returns a new database connection.
func newDb(u string) (db *PgDb, err error) {
	err = h.Retry(func() (e error) {

		db, e = NewDbWithOptions(u,
			15*time.Second,
			false,
			App.Config.DefaultInt("dbPoolSize", 5),
			0)
		if e != nil {
			time.Sleep(5 * time.Second)
		}
		return e
	}, MaxInt)
	return
}

// NewDbWithOptions returns a new database connection.
func NewDbWithOptions(u string, timeout time.Duration, retryStatementTimeout bool, poolSize, maxRetries int) (*PgDb, error) {
	o, err := pg.ParseURL(u)
	h.PanicOnError(err)
	o.MaxRetries = maxRetries
	o.RetryStatementTimeout = retryStatementTimeout
	o.PoolSize = poolSize
	o.DialTimeout = timeout
	o.ReadTimeout = timeout
	o.WriteTimeout = timeout
	db := pg.Connect(o)

	_, err = db.Exec("SELECT 1")
	if err != nil {
		logs.Error("Error connecting to Postgres %v", err)
		return nil, err
	}
	return &PgDb{db}, err
}

// DB returns a new database connection.
func DB() (db *sql.DB) {
	err := h.Retry(func() (e error) {
		db, e = sql.Open(
			"postgres",
			dbURL,
		)
		if e != nil {
			time.Sleep(5 * time.Second)
		}
		return e
	}, MaxInt)
	if err != nil {
		logs.Error("Database connection error: %s", err)
	}
	return
}

// dbLogger is the database logger.
type dbLogger struct{}

// BeforeQuery is called before a query is executed.
func (d dbLogger) BeforeQuery(c context.Context, q *pg.QueryEvent) (context.Context, error) {
	return c, nil
}

// AfterQuery is called after a query is executed.
func (d dbLogger) AfterQuery(c context.Context, q *pg.QueryEvent) error {
	query, err := q.FormattedQuery()
	h.PanicOnError(err)
	logs.Debug("%s", query)
	return err
}

// Que returns a new que client.
func Que(maxConnections int32) (*que.Client, *pgxpool.Pool, error) {
	connPoolConfig, err := pgxpool.ParseConfig(dbURL)
	h.PanicOnError(err)
	connPoolConfig.MaxConns = maxConnections
	connPoolConfig.AfterConnect = que.PrepareStatements

	p, err := pgxpool.ConnectConfig(context.Background(), connPoolConfig)
	if err != nil {
		return nil, nil, err
	}
	return que.NewClient(p), p, nil
}
