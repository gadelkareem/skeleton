package kernel

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/config"
	"github.com/astaxie/beego/context"
	"github.com/astaxie/beego/logs"
	"github.com/gadelkareem/go-helpers"
)

const (
	// Environment variables.
	Dev     = "dev"
	Prod    = "prod"
	Test    = "test"
	Staging = "staging"
	// ListLimit is the default limit for paginated lists.
	ListLimit = 10
	// MaxInt is the maximum value of an int.
	MaxInt = int(^uint(0) >> 1)
	// BinaryName is the name of the binary.
	BinaryName = "backend"
	// SiteName is the name of the site.
	SiteName = "Skeleton"
	// APIVersion is the version of the API.
	APIVersion = "v1"
)

type (
	// App is the application struct.
	app struct {
		Config                                      config.Configer
		RunMode, Host, FrontEndURL, APIURL, APIPath string
		DisableCache, EnableControls, IsCLI         bool
	}
	// Controller is the base controller.
	Controller struct {
		beego.Controller
	}
	// ControllerInterface is the interface for controllers.
	ControllerInterface interface {
		beego.ControllerInterface
	}
	// Command is the interface for commands.
	Command interface {
		Run(args []string)
		Help()
	}
)

var (
	// App is the application.
	App *app
	// Commands is the list of commands.
	Commands map[string]Command
	// TrustedIPs is the list of trusted IPs.
	TrustedIPs = []string{
		"127.0.0.1",
		"::1",
	}
	// allowedHosts is the list of allowed hosts.
	allowedHosts = []string{
		"skeleton-gadelkareem.herokuapp.com",
	}
)

// Bootstrap initializes the application.
func Bootstrap() {
	if App != nil {
		return
	}

	configMode := os.Getenv("BEEGO_RUNMODE")
	if configMode == "" {
		configMode = Prod
	}
	var (
		path string
		err  error
	)
	// go test
	if strings.HasSuffix(os.Args[0], ".test") {
		configMode = Test
		path = filepath.Join(os.Getenv("PWD"), "..")
		for {
			if _, err := os.Stat(filepath.Join(path, "conf")); err == nil {
				break
			}
			path = filepath.Join(path, "..")
		}
	} else {
		path, err = filepath.Abs(filepath.Dir(os.Args[0]))
		h.PanicOnError(err)
	}

	err = os.Chdir(path)
	h.PanicOnError(err)
	os.Args[0] = filepath.Join(path, BinaryName)

	handleProdConfig(configMode)

	err = beego.LoadAppConfig("ini", "conf/app."+configMode+".ini")
	h.PanicOnError(err)

	App = &app{
		Config: beego.AppConfig,
	}

	App.RunMode = App.Config.DefaultString("runmode", Prod)
	beego.BConfig.RunMode = App.RunMode
	beego.BConfig.Listen.HTTPAddr = App.ConfigOrEnvVar("httpaddr", "HTTP_ADDR")
	beego.BConfig.Listen.HTTPPort = App.ConfigOrEnvInt("httpport", "PORT")
	App.EnableControls = App.RunMode != Prod && App.Config.DefaultBool("enableControls", false)
	App.DisableCache = App.EnableControls && App.Config.DefaultBool("disableCache", false)
	// host
	App.Host = App.ConfigOrEnvVar("host", "SKELETON_HOST")
	allowedHosts = append(allowedHosts, App.Host)
	App.FrontEndURL = App.ConfigOrEnvVar("frontEndURL", "SKELETON_FRONTEND")
	App.APIURL = App.Config.String("apiURL")
	App.APIPath = App.Config.String("apiPath")

	// db
	initDBConfig()

}

// handleProdConfig handles the production config.
func handleProdConfig(m string) {
	if m != Prod {
		return
	}
	p := "conf/app.prod.ini.secret"
	if h.FileExists(p) {
		return
	}
	s := os.Getenv("PROD_CONFIG_SECRET_FILE")
	var err error
	if s == "" {
		s, err = h.ReadFile("conf/app.dev.ini.secret.example")
		h.PanicOnError(err)
	} else {
		s, err = h.Base64Decode(s)
		h.PanicOnError(err)
	}
	err = h.WriteFile(p, s)
	h.PanicOnError(err)
}

// ConfigOrEnvVar returns the config value or the environment variable.
func (a *app) ConfigOrEnvVar(k, e string) string {
	s := os.Getenv(e)
	if s == "" {
		return App.Config.String(k)
	}
	return s
}

// ConfigOrEnvInt returns the config value or the environment variable.
func (a *app) ConfigOrEnvInt(k, e string) int {
	s := os.Getenv(e)
	if s == "" {
		return App.Config.DefaultInt(k, 0)
	}
	i, _ := strconv.Atoi(s)
	return i
}

// SetupServer sets up the server.
func (a *app) SetupServer(errController ControllerInterface) {
	// leave it high until https://github.com/golang/go/issues/16100
	beego.BConfig.Listen.ServerTimeOut = 15
	beego.ErrorController(errController)

	a.logging()

	if !IsDev() {
		beego.DelStaticPath("/static")
		beego.DelStaticPath("/favicon.ico")
	}
	beego.SetStaticPath("/", "dist")
	beego.InsertFilter("*", beego.BeforeStatic, func(c *context.Context) {
		p := c.Request.URL.Path
		// CORS
		if c.Input.Method() == http.MethodOptions {
			SetCORS(c)
			c.Output.SetStatus(http.StatusOK)
			logs.AccessLog(&logs.AccessLogRecord{
				RequestMethod: http.MethodOptions,
				Request:       c.Request.URL.String(),
				Host:          c.Request.Host,
				HTTPReferrer:  c.Request.Referer(),
				HTTPUserAgent: c.Request.UserAgent(),
				Status:        http.StatusOK,
			}, "")
			panic(beego.ErrAbort)
		} else if c.Input.Method() == http.MethodGet &&
			!strings.HasSuffix(p, "/") && !strings.HasPrefix(p, "/api") && filepath.Ext(p) == "" {
			c.Request.URL.Path += "/"
			c.Redirect(http.StatusMovedPermanently, c.Request.URL.String())
			panic(beego.ErrAbort)
		}
	}, true)
}

// SetCORS sets the CORS headers.
func SetCORS(c *context.Context) {
	c.Output.Header("Access-Control-Allow-Origin", App.FrontEndURL)
	c.Output.Header("Access-Control-Allow-Headers", "content-type,authorization,x-requested-with")
	c.Output.Header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,PATCH,OPTIONS")
	c.Output.Header("Access-Control-Allow-Credentials", "true")
}

// logging sets up the logging.
func (a *app) logging() {
	if App.RunMode == Prod {
		err := logs.GetBeeLogger().DelLogger(logs.AdapterConsole)
		h.PanicOnError(err)
		err = logs.SetLogger(logs.AdapterConsole, `{"color":false}`)
		h.PanicOnError(err)
	} else {
		logs.EnableFuncCallDepth(true)
	}
	logs.SetLevel(App.Config.DefaultInt("logLevel", logs.LevelWarning))
}

// Run runs the app.
func (a *app) Run(params ...string) {
	beego.Run(params...)
}

// RunCommand runs the command.
func (a *app) RunCommand() {
	isHelp := func(s string) bool {
		return s == "help" || s == "-h" || s == "--help"
	}
	if len(os.Args) < 2 {
		return
	}
	args := os.Args
	if isHelp(args[1]) {
		fmt.Printf("Available Commands:\n")
		for k, cmd := range Commands {
			fmt.Printf("----- %s:", k)
			cmd.Help()
		}
		os.Exit(0)
	}
	if cmd, ok := Commands[args[1]]; ok {
		if len(args) > 2 && isHelp(args[2]) {
			cmd.Help()
			os.Exit(0)
		}
		a.IsCLI = true
		cmd.Run(args[2:])
		os.Exit(0)
	}
}

// IsDev returns true if the app is in dev mode.
func IsDev() bool {
	if App == nil {
		return true
	}
	return App.RunMode == Dev
}

// IsIPTrusted returns true if the IP is trusted.
func IsIPTrusted(ip string) bool {
	for _, v := range TrustedIPs {
		if ip == v {
			return true
		}
	}
	return false
}

// IsHostAllowed returns true if the host is allowed.
func IsHostAllowed(host string) bool {
	if App.RunMode != Prod {
		return true
	}
	for _, a := range allowedHosts {
		if host == a {
			return true
		}
	}
	return false
}
