#!/usr/bin/env bash

# test.sh

# This script runs the test suite for the current project. It is used
# to run the test suite in the CI pipeline.



// TestTest runs the test binary with the -v flag and a count of 1.
go test -v -count=1 $@
